const btn = document.querySelector('.menu-btn'); 
const menu = document.querySelector('.header__navbar');
const closeBtn = document.querySelector('.close-icon-wrapper');



const showMenu = function () {
    menu.classList.toggle('header__navbar--isActive')
    closeBtn.classList.toggle('close-icon-wrapper--disabled') 
}

btn.addEventListener('click', showMenu);
