Project: https://eugenebeldugin.github.io/forkio-page/

Над проектом Step Project Forkio трудилась молодая команда будущих Front-end разработчиков: 
* Team-lead - Daria Molchanova
* Project arcitector - Evgen Beldugin

Наша молодая команда использовала данный список технологий:
    <ul> Технологии 
        <li> сборщик проекта Gulp </li>
        <li> препроцессор Sass </li>
        <li> Adaptive-responsive верстку </li>
        <li> Работа в команде благодаря таким приложениям, как GitLab, Telegram, Zoom-meeting, Mob-Calls </li>
    </ul>

Какие задачи выполнял кто из участников:

Daria Molchanova VS Evgen Beldugin:
* Создание структуры проекта 
* Определение общих правил написания кода
* Создание @mixin, $variables

Daria Molchanova: 
* Верстка шапки сайта с верхним меню 
* Выпадающее меню при малом разрешении экрана
* Верска секции People Are Talking About Fork.

Evgen Beldugin:
* Верстка блока Revolutionary Editor. 
* Кнопки watch/star/fork 
* Верстка секции Here is what you get.
* Верстка секции Fork Subscription Pricing.




